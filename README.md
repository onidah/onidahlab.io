# Onidah Project

The Onidah Project aims to create a space for research and writing toward a Mormon Liberation Theology. Insomuch as our church relies on a lay clergy, so will this project rely on contributions from our community. Theology as conceived here is an active and open dialogue about our faith with a commitment to rigor and honesty in reading and research. 

If you're interested in contributing to this project in any way then please do. 

## How to contribute 

There are a variety of ways people can contribute to this project. Though the most visible contribution might look like an essay or post we also need community members to notify us of errors, ask questions, and even criticize the way we're working together. 

We will be using GitLab for this project. It is cost effective (free right now) and also offers the benefit of transparency into the work being done. 

### Submitting an Issue

If you have a **question about the project** please submit using [this link.](https://gitlab.com/onidah/onidah.gitlab.io/issues/new?issuable_template=project_question)


If you have a **question about a post or a topic** that would be relevant to a Mormon Liberation Theology please submit using [this link.](https://gitlab.com/onidah/onidah.gitlab.io/issues/new?issuable_template=research_question)

If you have **feedback** because you found an error or think there is something we could do better please submit using [this link.](https://gitlab.com/onidah/onidah.gitlab.io/issues/new?issuable_template=feedback)

### Submitting a Merge Request

While community members are always invited to submit issues the fastest and most compelling way to make a change to the project is to submit a merge request.

### Submitting a Pos
For now, just create a merge request with the markdown file in `/content/posts/` and we'll review the content there. 

