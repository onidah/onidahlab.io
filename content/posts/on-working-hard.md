---
title: "On Working Hard"
date: 2019-12-27T16:41:34-07:00
draft: false
---

A lot of discussion on Twitter yesterday about working nights and weekends in your twenties. Most of the discussion I saw revolved around this tweet from the owner of a cryptocurrency company. 

![tweet](/img/ScreenShot_selkis_tweet.png)

And this hideous take about the response to it.

![retweet](/img/ScreenShot_hoffman_retweet.png)

There is a lot that could and should be said about our relationship to work, both socially and individually. It's hard for me to suppress a rant here about Thatcher or the *absurd* equivocation of working for profit and working for human rights

But, as a young father who works at a tech company and who has felt the hierarchical pressure to work nights and weekends at the abandonment of my wife and children I feel most compelled to write a little about how this notion of hard work rubs against the sort of work a Latter-day Saint has covenanted to do. I don't intend to answer many questions with this short post. My hope is really just to introduce the conflict between capital and Zion and how we cannot have both. 

## Hugh Nibley's Take

This apparently isn't a new discussion within the mormon world. Hugh Nibley seems to address it directly in Approaching Zion.

>“First, of course, the work ethic, which is being so strenuously advocated in our day. This is one of those neat magician’s tricks in which all our attention is focused on one hand while the other hand does the manipulating. Implicit in the work ethic are the ideas (1) that because one must work to acquire wealth, work equals wealth, and (2) that that is the whole equation. With these go the corollaries that anyone who has wealth must have earned it by hard work and is, therefore, beyond criticism; that anyone who doesn’t have it deserves to suffer—thus penalizing any who do not work for money; and (since you have a right to all you earn) that the only real work is for one’s self; and, finally, that any limit set to the amount of wealth an individual may acquire is a satanic device to deprive men of their free agency—thus making mockery of the Council of Heaven.
>
> These editorial syllogisms we have heard a thousand times, but you will not find them in the scriptures. Even the cornerstone of virtue, “He that is idle shall not eat the bread . . . of the laborer” (D&C 42:42), hailed as the franchise of unbridled capitalism, is rather a rebuke to that system which has allowed idlers to live in luxury and laborers in want throughout the whole course of history. The whole emphasis in the holy writ is not on whether one works or not, but what one works for: “The laborer in Zion shall labor for Zion; for if they labor for money they shall perish” (2 Nephi 26:31). “The people of the church began to wax proud, because of their exceeding riches, . . . precious things, which they had obtained by their industry” (Alma 4:6) and which proved their undoing, for all their hard work.
>
> **In Zion you labor, to be sure, but not for money, and not for yourself, which is the exact opposite of our present version of the work ethic.”**

Martin Luther King Junior worked toward the civil liberation of Black Americans. Ghandi worked against British Imperialism. 

Selkis worked for his own *profit* through the *commodification* of scarcity.

The reason Selkis and Hoffman can't understand the objection to their tweets is that both have embraced a nihilism of capital. They value 'change' and 'success' without any moral obligation. They probably sincerely believe in their vision to change the world or have a successful career, but neither of these ends entail any actual value; not any any sense that would be compatible with Zion or The Kingdom of God. 

Hopefully this project can work towards making more accessible an analysis of this nihilism and it's conflict with a Christian morality. My wife needs help with dinner and I need to put this down for today. We have a lot of work ahead of us. 



