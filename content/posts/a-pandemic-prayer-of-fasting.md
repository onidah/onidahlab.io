---
title: "A Pandemic Prayer of Fasting"
date: 2020-04-10T11:23:54-07:00
draft: false
---
Father, 

I find myself, this Good Friday in a fast. Fasting not only with hope and faith in a better future and fasting not only for comfort and support for those who have lost their lives, loved ones, health, and livelihoods, but fasting for righteous judgement. 

I am fasting also for justice.

Lord, why do you make me see wrongdoing while the law is slack and justice seems to never prevail? The wicked have overpowered the humble; the judgments of our kings are perverse and set on vain gain. Why do we fast and you seem unaware? Where are your prophets? Do we seek only our interest in a day of fasting while the humble and oppressed lose their meager hold? 

Father, be just! 

Lord hear our fast today and, Lord, loose the bands of wickedness, undo the heavy burdens, and let the oppressed go free that you break every yoke! Help us to feed the hungry, to give the houseless shelter, to cover the naked. Do not let us hide from our kin! Their sweat and their blood cry out while the spoil of their work us in our homes. 

Lord show us your marvelous work and your wonder and make it the work of our hands too. Give us the speed and strength to brick by brick by board by board build your kingdom and your glory. Give us the courage to *believe* and to *see* the kingdom here among us. 

Give us the patience to endure the work that we might not only be called your sheep but that we may bring all of your children with us. 

Father, deal justice to those who fight against Zion, whether in greed or comfort. Make us to see the profanity of our security. I need patience, Lord, give me your patience. 

Father, let your light break forth like the dawn and your healing spring up quickly, that your righteous may be redeemed and called to the reward that we may all feed with the heritage of Jacob. 

Father hear our love and give us more of it in the name Jesus Christ. Amen. 
