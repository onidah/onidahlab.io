---
title: "A Project of Mormon Liberation Theology"
date: 2019-12-10T17:50:03-06:00
draft: false
---

>The poor person does not exist as an inescapable fact of destiny. His or her existence is not politically neutral, and it is not ethically innocent. The poor are a by-product of the system in which we live and for which we are responsible. They are marginalized by our social and cultural world. They are the oppressed, exploited proletariat, robbed of the fruit of their labor and despoiled of their humanity. Hence the poverty of the poor is not a call to generous relief action, but a demand that we go and build a different social order. 
> -- Gustavo Gutiérrez

## A Liberation Theology

A liberation theology is a theology that focuses on the marginalized of the world in anticipation of and in the work towards the establishment of The Kingdom Of God. It is the practical idea that, as Christians, we have committed to serve the poor and the oppressed and that we will ultimately be judged by this commitment. Ultimately the goal of such a theology is the goal of Zion which entails the abolition of poverty itself.


## A Mormon Liberation Theology

We have, in our own Mormon faith, a rich tradition of work that aims at the establishment of the Kingdom of God where [poverty is abolished](https://www.churchofjesuschrist.org/study/scriptures/pgp/moses/7.18?lang=eng#p18) as in Enoch's Zion. The name Onidah comes from Alma 32 where the poor where alienated from the synagogues [that they had built](https://www.churchofjesuschrist.org/study/scriptures/bofm/alma/32?lang=eng#p5); it was the place they gathered to hear the Gospel and it was their humility which qualified them for hearing the word of God. 

The goal of this journal is to be a place similar, a place for the humble, poor, and marginalized to be heard and considered in our ministry as disciples of Jesus Christ. 

> Who can be 'agents unto themselves' if they are in bondage to others and have to accept their terms?
> ― Hugh Nibley


---


## Contributing

If you'd like to contribute to the project please visit it [on GitLab](https://gitlab.com/rhodakanaty/onidah) where we have a README.